from tkinter.constants import NO
class Nodo:
    def __init__(self,dato=None):
        self.dato=dato
        self.nodoSiguiente=None
    def getdato(self):
        return self.dato
    def getnodoSiuiente(self):
        return self.nodoSiguiente
    def setDato(self,dato):
        self.dato=dato
    def setNodoSiguiente(self,siguiente):
        self.nodoSiguiente=siguiente
    def __str__(self):
        return f"Nodo dato= {self.dato} Nodo siguiente {self.nodoSiguiente}"
class ListaEnlazada():
    def __init__(self):
        self.nodoInicio = Nodo()
        self.nodoFinal= Nodo()
        self.nodoInicio.nodoSiguiente= self.nodoFinal
        self.numeroElementos=0
    def listaVacia(self):
        if(self.numeroElementos==0):
            return True
        else:
            return False
    def vaciarLista(self):
        self.nodoInicio=self.nodoFinal=None
        self.numeroElementos=0
    def conexion(self,conectorA,receptor,conectorB):
        conectorA.nodoSiguiente = receptor
        receptor.nodoSiguiente = conectorB
        self.numeroElementos=self.numeroElementos + 1
    def insertarElementoInicio(self,dato):
        self.conexion(self.nodoInicio,Nodo(dato),self.nodoInicio.nodoSiguiente)
    def insertarElementoFinal(self,dato):
        auxiliar=self.nodoInicio
        while auxiliar.nodoSiguiente != self.nodoFinal:
            auxiliar = auxiliar.nodoSiguiente        
        self.conexion(auxiliar,Nodo(dato),auxiliar.nodoSiguiente)
    def mostrarLista(self):
        if self.listaVacia()==False:
            temporal=self.nodoInicio.nodoSiguiente
            print("\nLista de elementos:")
            while temporal!=self.nodoFinal:
                print(f"[{temporal.dato}]--> ", end="")
                temporal=temporal.nodoSiguiente
        else:
            print("\nLa lista esta vacia")
    def eliminarElementoInicio(self):
        if(self.verificarVacia()==False):
            self.nodoInicio.nodoSiguiente=self.nodoInicio.nodoSiguiente.nodoSiguiente
            self.numeroElementos = self.numeroElementos-1
        else:
            print("\nError al eliminar: La lista esta vacia")
    def eliminarElementoFinal(self):
        if(self.verificarVacia()==False):
            elemento = self.nodoInicio
            temporal = None
            while elemento.nodoSiguiente!=self.nodoFinal:
                temporal = elemento
                elemento=elemento.nodoSiguiente
            temporal.nodoSiguiente = self.nodoFinal
            self.numeroElementos = self.numeroElementos-1
        else:
            print("\nError al eliminar: La lista esta vacia")
    def mostrarCantidadElementos(self):
        print(f"Cantidad de elementos: {self.numeroElementos}")
    pass
elec = ""
lista = ListaEnlazada()
while(elec!="8"):
    elec = input("\nPrograma de listas enlazadas\n\n1.- Verificar lista vacia\n2.- Mostrar elementos\n3.- Insertar elemento al inicio\n4.- Insertar elemento al final\n5.- Eliminar elemento al inicio\n6.- Eliminar elemento al final\n7.- Mostrar cantidad de elementos\n8.- Salir\n")
    if(elec=="1"):
        if(lista.verificarVacia()):
            print("\nLa lista esta vacia")
        else:
            print("\nLa contiene elementos")
    elif(elec=="2"):
        lista.mostrarLista()
    elif(elec=="3"):
        dato = input("\nInserte dato que desea agregar al inicio:")
        lista.insertarElementoInicio(dato)
    elif(elec=="4"):
        dato = input("\nInserte dato que desea agregar al final:")
        lista.insertarElementoFinal(dato)
    elif(elec=="5"):
        lista.eliminarElementoInicio()
    elif(elec=="6"):
        lista.eliminarElementoFinal()
    elif(elec=="7"):
        lista.mostrarCantidadElementos()